const fs = require('fs')

const test = process.argv.slice(2)[0]
const data = fs.readFileSync((test !== undefined ? `./${test}` : './test1.txt'), 'utf-8').split('\n').map(line => line.replace(/\s/g, ''))
const directions = [[1, 0], [-1, 0], [0, 1], [0, -1], [1, 1], [-1, 1], [1, -1], [-1, -1]];
const grid = data.slice(1, parseInt(data[0].split('x')[1]) + 1).map(line => [...line])
const words = data.slice(parseInt(data[0].split('x')[1]) + 1)
let charMap = {}

// preprocesses grid into map of letters to coordinates
for (let x = 0; x < grid[0].length; x++) {
  for (let y = 0; y < grid.length; y++) {
    if (Object.keys(charMap).includes(grid[y][x])) {
      charMap[grid[y][x]].push([y, x])
    }
    else {
      charMap[grid[y][x]] = [[y, x]]
    }
  }
}

// begins searches at occurences of first letters
words.map((word) => {
  let firstLetterCoords = charMap[[...word][0]]

  firstLetterCoords.map(start => {
    directions.map(dir => {
      match(word, 1, start, [start[0] + dir[0], start[1] + dir[1]], dir)
    })
  })
})

/** 
 * Checks if the next proposed coordinate (end) is valid
*/
function match(word, letterNum, start, end, dir) {
  let possibleCoords = charMap[[...word][letterNum]]

  if (possibleCoords.map(c => c.toString()).includes(end.toString())) {
    // checks if we've reached the end of the word, otherwise continues matching
    if (letterNum == word.length - 1) {
      console.log(`${word} ${start[0]}:${start[1]} ${end[0]}:${end[1]}`)
    }
    else {
      match(word, letterNum + 1, start, [end[0] + dir[0], end[1] + dir[1]], dir)
    }
  }
  else {
    return
  }
}
